/*
Navicat MySQL Data Transfer

Source Server         : localhost-mysql
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : maju_mundur

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-01-06 22:24:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_m_function_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_m_function_menu`;
CREATE TABLE `tb_m_function_menu` (
  `function_id` int(11) NOT NULL AUTO_INCREMENT,
  `function_parent` int(11) NOT NULL,
  `function_name` varchar(100) NOT NULL,
  `function_class_controller` varchar(100) NOT NULL,
  `function_active` int(11) NOT NULL,
  `function_order` int(10) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_dt` date NOT NULL,
  `changed_by` int(11) NOT NULL,
  `changed_dt` date NOT NULL,
  PRIMARY KEY (`function_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_m_function_menu
-- ----------------------------

-- ----------------------------
-- Table structure for tb_m_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_m_product`;
CREATE TABLE `tb_m_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_reward_id` int(11) NOT NULL,
  `name_product` varchar(11) DEFAULT '',
  `price` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_dt` date NOT NULL,
  `changed_by` int(11) NOT NULL,
  `changed_dt` date NOT NULL,
  PRIMARY KEY (`product_id`),
  KEY `FKtb_m_produ17552` (`type_reward_id`),
  CONSTRAINT `FKtb_m_produ17552` FOREIGN KEY (`type_reward_id`) REFERENCES `tb_m_type_reward` (`type_reward_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_m_product
-- ----------------------------
INSERT INTO `tb_m_product` VALUES ('1', '1', 'switter', '20000', '1', '2019-12-24', '0', '0000-00-00');

-- ----------------------------
-- Table structure for tb_m_type_reward
-- ----------------------------
DROP TABLE IF EXISTS `tb_m_type_reward`;
CREATE TABLE `tb_m_type_reward` (
  `type_reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `score_reward` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_dt` date NOT NULL,
  `changed_by` int(11) NOT NULL,
  `changed_dt` date NOT NULL,
  PRIMARY KEY (`type_reward_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_m_type_reward
-- ----------------------------
INSERT INTO `tb_m_type_reward` VALUES ('1', 'conditional', '10', '1', '2019-12-24', '0', '0000-00-00');
INSERT INTO `tb_m_type_reward` VALUES ('3', 'conditionals', '11', '1', '2020-01-06', '0', '0000-00-00');

-- ----------------------------
-- Table structure for tb_m_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_m_user`;
CREATE TABLE `tb_m_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(10) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `passwords` varchar(255) DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `birth_place` varchar(150) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt` date DEFAULT NULL,
  `changed_by` int(10) DEFAULT NULL,
  `changed_dt` date DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `FKtb_m_user512134` (`user_group_id`),
  CONSTRAINT `FKtb_m_user512134` FOREIGN KEY (`user_group_id`) REFERENCES `tb_m_user_group` (`user_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_m_user
-- ----------------------------
INSERT INTO `tb_m_user` VALUES ('1', '1', 'eri', 'd41d8cd98f00b204e9800998ecf8427e', null, '12121', null, null, '1', '0000-00-00', '1', '2020-01-06');
INSERT INTO `tb_m_user` VALUES ('2', '2', 'safari', '60190cb2b5a1d64c6c22fdda4d9e29e0', 'safari.erie@gmail.com', '085624455679', 'Bandung', null, null, '2019-12-20', null, null);

-- ----------------------------
-- Table structure for tb_m_user_group
-- ----------------------------
DROP TABLE IF EXISTS `tb_m_user_group`;
CREATE TABLE `tb_m_user_group` (
  `user_group_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_group_name` varchar(100) NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_m_user_group
-- ----------------------------
INSERT INTO `tb_m_user_group` VALUES ('1', 'merchant');
INSERT INTO `tb_m_user_group` VALUES ('2', 'customer');
INSERT INTO `tb_m_user_group` VALUES ('3', 'admin');

-- ----------------------------
-- Table structure for tb_m_user_group_auth
-- ----------------------------
DROP TABLE IF EXISTS `tb_m_user_group_auth`;
CREATE TABLE `tb_m_user_group_auth` (
  `group_auth_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(10) DEFAULT NULL,
  `function_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`group_auth_id`),
  KEY `FKtb_m_user_113900` (`user_group_id`),
  KEY `FKtb_m_user_130791` (`function_id`),
  CONSTRAINT `FKtb_m_user_113900` FOREIGN KEY (`user_group_id`) REFERENCES `tb_m_user_group` (`user_group_id`),
  CONSTRAINT `FKtb_m_user_130791` FOREIGN KEY (`function_id`) REFERENCES `tb_m_function_menu` (`function_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_m_user_group_auth
-- ----------------------------

-- ----------------------------
-- Table structure for tb_trx_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_trx_product`;
CREATE TABLE `tb_trx_product` (
  `trx_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt` date DEFAULT NULL,
  `changed_by` int(11) DEFAULT NULL,
  `changed_dt` date DEFAULT NULL,
  PRIMARY KEY (`trx_product_id`),
  KEY `FKtb_trx_pro890479` (`product_id`),
  KEY `FKtb_trx_pro526353` (`user_id`),
  CONSTRAINT `FKtb_trx_pro526353` FOREIGN KEY (`user_id`) REFERENCES `tb_m_user` (`user_id`),
  CONSTRAINT `FKtb_trx_pro890479` FOREIGN KEY (`product_id`) REFERENCES `tb_m_product` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_trx_product
-- ----------------------------
INSERT INTO `tb_trx_product` VALUES ('1', '1', '2', null, null, null, null);
INSERT INTO `tb_trx_product` VALUES ('2', '1', '2', null, null, null, null);
INSERT INTO `tb_trx_product` VALUES ('3', '1', '2', null, null, null, null);
