<?php

/* Created : safari.erie@gmail.com
 * Created_dt : 2019122-
 *
 * TODO
 * Post query login
 * retrieve query menus

 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20191220    Starting Class models 
 * 2      20191220    Create Method get data users by username function for login
 * 3      20191220    Create Method get user by user id, inqury for update
 * 4.     20191221    Create Method for Insert data user
 * 5.     20191221    Create Method for Update data user
 * */

class Users_model extends CI_Model 
{
    function __construct() 
    {
        parent:: __construct();
    }

    function get_users_by_username($username){

        // query to tb_m_users by email
        $sql = "
            SELECT
                user_id,
                user_group_id,
                username,
                passwords,
                telephone,
                birth_date,
                birth_place
            FROM
                tb_m_user
            WHERE username = '".$username."'
        ";

        $users = $this->db->query($sql);
        // check confdition null or not null
        if($users->num_rows() > 0){
            return $users->row();
        }else{
            return false;
        }
    }

    function get_user_by_id($user_id){
        $sql = "
            SELECT
                user_id,
                user_group_id,
                username,
                passwords,
                telephone,
                birth_date,
                birth_place
            FROM
                tb_m_user
            WHERE user_id = $user_id
        ";

        $users = $this->db->query($sql);
        // check confdition null or not null
        if($users->num_rows() > 0){
            return $users->row();
        }else{
            return false;
        }

    }

    function get_user_group_by_id($id){

        $sql = "
            SELECT
                user_group_id,
                user_group_name
            FROM
                tb_m_user_group
            WHERE
                user_group_id = $id
        ";

        $dt_user_group = $this->db->query($sql);
        if($dt_user_group->num_rows() > 0){
            return $dt_user_group->row();
        }else{
            return false;
        }
    }

    function insert_update_users($data_users,$user_id){

        // condition insert
        if($user_id == '' || $user_id == null){
            $this->db->insert('tb_m_user',$data_users);
            return ($this->db->affected_rows() != 1) ? false : true;
        }else{
            $this->db->where('user_id',$user_id);
            $this->db->update('tb_m_user',$data_users);
            return ($this->db->affected_rows() != 1) ? false : true;
        }

    }


}