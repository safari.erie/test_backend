<?php

/* Created : safari.erie@gmail.com
 * Created_dt : 2019122-
 *
 * TODO
 * Post query login
 * retrieve query menus

 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20191221    Starting Class models 
 * 2      20191221    Create Method get data master Product
 * 3      20191221    Create Method get Insert and update master product
 * */

class Transaction_model extends CI_Model 
{

    function buy_product($data){

        $this->db->insert('tb_trx_product',$data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    function get_transaction($user_id){
        $sql = "
            SELECT
                trx.trx_product_id,
                trx.product_id,
                trx.user_id,
                u.username,
                u.email,
                u.telephone,
                u.birth_place,
                p.name_product,
                p.price,
                tr.score_reward
            FROM
                tb_trx_product trx
            INNER JOIN tb_m_user u ON u.user_id = trx.user_id
            INNER JOIN tb_m_product p ON p.product_id = trx.product_id
            INNER JOIN tb_m_type_reward tr ON tr.type_reward_id = p.type_reward_id
        ";

        if($user_id != ''){
            $sql  = $sql." WHERE trx.user_id = $user_id";
        }

        $data = $this->db->query($sql)->result();
        return $data;
    }
}