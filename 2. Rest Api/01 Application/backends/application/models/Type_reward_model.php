<?php

/* Created : safari.erie@gmail.com
 * Created_dt : 2019122-
 *
 * TODO
 * Post query login
 * retrieve query menus

 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20191221    Starting Class models 
 * 2      20191221    Create Method get data master Type Reward 
 * 3      20191221    Create Method get user by user id, inqury for update
 * 4.     20191221    Create Method for Insert data user
 * 5.     20191221    Create Method for Update data user
 * */

class Type_reward_model extends CI_Model 
{
    function __construct(){
        
    }

    function getAllTypeReward(){
        $sql = "
            SELECT
                type_reward_id,
                NAME,
                score_reward,
                created_by,
                created_dt,
                changed_by,
                changed_dt
            FROM
                tb_m_type_reward
        ";

        $data = $this->db->query($sql)->result();

        return $data;

    }

    function getTypeRewardId($type_reward_id){
        $sql = "
            SELECT
                type_reward_id,
                NAME,
                score_reward,
                created_by,
                created_dt,
                changed_by,
                changed_dt
            FROM
                tb_m_type_reward
            where type_reward_id = $type_reward_id
        ";

        $data = $this->db->query($sql);

        if($data->num_rows() > 0){
            return $data->row();
        }else{
            return false;
        }
    }

    function insert_update($data,$id){

        // condition insert
        if($id == '' || $id == null){
            $this->db->insert('tb_m_type_reward',$data);
            return ($this->db->affected_rows() != 1) ? false : true;
        }else{
            /* */
            $this->db->where('type_reward_id',$id);
            $this->db->update('tb_m_type_reward',$data);
            return ($this->db->affected_rows() != 1) ? false : true;
        }

    }
    
}