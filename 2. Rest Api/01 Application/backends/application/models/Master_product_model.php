<?php

/* Created : safari.erie@gmail.com
 * Created_dt : 2019122-
 *
 * TODO
 * Post query login
 * retrieve query menus

 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20191221    Starting Class models 
 * 2      20191221    Create Method get data master Product
 * 3      20191221    Create Method get Insert and update master product
 * */

class Master_product_model extends CI_Model 
{
    function __construct(){
        
    }

    function getAllMasterProduct(){
        $sql = "
                SELECT
                    product_id,
                    type_reward_id,
                    name_product,
                    price,
                    created_by,
                    created_dt,
                    changed_by,
                    changed_dt
                FROM
                tb_m_product
        ";

        $data = $this->db->query($sql)->result();

        return $data;

    }

    function getProductById($product_id){
        $sql = "
                SELECT
                    product_id,
                    type_reward_id,
                    name_product,
                    price,
                    created_by,
                    created_dt,
                    changed_by,
                    changed_dt
                FROM
                tb_m_product
                where product_id = $product_id
        ";

        $data = $this->db->query($sql);

        if($data->num_rows() > 0){
            return $data->row();
        }else{
            return false;
        }
    }

    function insert_update($data,$id){

        // condition insert
        if($id == '' || $id == null){
            $this->db->insert('tb_m_product',$data);
            return ($this->db->affected_rows() != 1) ? false : true;
        }else{
            /* */
            $this->db->where('product_id',$id);
            $this->db->update('tb_m_product',$data);
            return ($this->db->affected_rows() != 1) ? false : true;
        }

    }
    
}