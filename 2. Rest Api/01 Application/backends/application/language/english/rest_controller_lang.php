<?php

/*
 * English language
 */

$lang['title'] = 'Smartfix English';
$lang['text_rest_invalid_api_key'] = 'Invalid API key %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_ip_address_time_limit'] = 'This IP Address has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';

$lang['text_rest_unregistered_user'] = 'Unregistered User';
$lang['text_rest_session_expired'] = 'Session Expired';
$lang['text_rest_enter_email_login'] = 'Enter your email';
$lang['text_rest_enter_password_login'] = 'Enter your password';
$lang['text_rest_forgot_password_login'] = 'Forgot password ?';
$lang['text_rest_search_models_search_models'] = 'Search models';
$lang['text_rest_functional_areas_selected_model'] = 'Functional areas';
$lang['text_rest_conditions_selected_model'] = 'Conditions';
$lang['text_rest_issues_issues_switch'] = 'Issues';
$lang['text_rest_apply_conditions'] = 'Apply';
$lang['text_rest_back_to_functional_areas_issues'] = 'Back to functional areas';
$lang['text_rest_back_to_issues_procedures'] = 'Back to issues';
$lang['text_rest_procedures_procedures'] = 'Procedures';
$lang['text_rest_times_solved_procedures'] = 'Times solved';
$lang['text_rest_back_to_procedures'] = 'Back to procedures';
$lang['text_rest_diagnostic_step'] = 'Diagnostic step';
$lang['text_rest_yes_repair'] = 'Yes, go to repair step';
$lang['text_rest_no_repair'] = 'No, go to next diagnostic step';
$lang['text_rest_back_diagnostic'] = 'Back to diagnostic step';
$lang['text_rest_repair_step'] = 'Repair step';
$lang['text_rest_validation_repair_step'] = 'Validation Step';
$lang['text_rest_back_repair'] = 'Back to repair step';
$lang['text_rest_validation_step_validation_step'] = 'Validation Step';
$lang['text_rest_not_solved_validation_step'] = 'Not Solved';
$lang['text_rest_solved_validation_step'] = 'Solved';
$lang['text_rest_issue_solved_solved'] = 'Issue Solved';
$lang['text_rest_back_home_solved'] = 'Back to home';
$lang['text_rest_issue_not_solved'] = 'Issue not solved';
$lang['text_rest_back_procedures'] = 'Back to procedures';
$lang['text_rest_users_side_menu'] = 'Users';
$lang['text_rest_models_side_menu'] = 'Models';
$lang['text_rest_issues_side_menu'] = 'Issues';
$lang['text_rest_profile_side_menu'] = 'Profile';
$lang['text_rest_name_users'] = 'Name';
$lang['text_rest_email_users'] = 'E-mail';
$lang['text_rest_role_users'] = 'Role';
$lang['text_rest_invited_users'] = 'Invited';
$lang['text_rest_options_users'] = 'Options';
$lang['text_rest_yes_users'] = 'Yes';
$lang['text_rest_no_users'] = 'No';
$lang['text_rest_edit_users'] = 'Edit';
$lang['text_rest_delete_users'] = 'Delete';
$lang['text_rest_search_users'] = 'Search...';
$lang['text_rest_model_models'] = 'Model';
$lang['text_rest_type_models'] = 'Type';
$lang['text_rest_constr_year'] = 'Constr.year';
$lang['text_rest_product_category'] = 'Product Category';
$lang['text_rest_options_models'] = 'Options';
$lang['text_rest_variant'] = 'Variant';
$lang['text_rest_edit_models'] = 'Edit';
$lang['text_rest_delete_models'] = 'Delete model';
$lang['text_rest_search_models'] = 'Search';
$lang['text_rest_issues_issues'] = 'Issues';
$lang['text_rest_issue_issues'] = 'Issue';
$lang['text_rest_procedures_issues'] = 'Procedures';
$lang['text_rest_functional_areas_issues'] = 'Functional areas';
$lang['text_rest_models_issues'] = 'Models';
$lang['text_rest_add_issue_issues'] = 'Add Issue';
$lang['text_rest_search_issues'] = 'Search';
$lang['text_rest_create_issue'] = 'Once you have created an issue, you can then add procedures ';
$lang['text_rest_attach_issue'] = 'Attach Models';
$lang['text_rest_selected_issue'] = 'Selected';
$lang['text_rest_insert_issue'] = 'Save';
$lang['text_rest_manage_issue'] = 'Manage';
$lang['text_rest_add_procedure'] = 'Add procedure';
$lang['text_rest_add_block_procedure'] = 'Add block';
$lang['text_rest_type_diagnostic_procedure'] = 'Type: Diagnotic step';
$lang['text_rest_type_repair_procedure'] = 'Type: Repair step';
$lang['text_rest_delete_procedure'] = 'Delete';

//companies
$lang['text_rest_user_success'] = 'Success';
$lang['text_rest_company_name_required'] = 'Company name is required';
$lang['text_rest_company_max_length'] = 'Company Name length maximum 100';
$lang['text_rest_interface_lang_max_length'] = 'Interface Language length maximum 5';
$lang['text_rest_company'] = 'Company';
$lang['text_rest_exists'] = 'already exists';
$lang['text_rest_company_new'] = 'New Company Succesfully Created';
$lang['text_rest_company_edit'] = 'Company Succesfully Edited';
$lang['text_rest_company_undefined'] = 'Undefined Company';
$lang['text_rest_company_not_found'] = 'Company ID Not found';
$lang['text_rest_company_deleted'] = 'Company Succesfully Deleted';
$lang['text_rest_company_fail_deleted'] = 'Company fail to deleted with error';

//login
$lang['text_rest_email_required'] = 'Email is required';
$lang['text_rest_email_found'] = 'Email found';
$lang['text_rest_email_not_found'] = 'Email not found';
$lang['text_rest_email_not_register'] = 'The email you entered is not registered';
$lang['text_rest_reset_password_sent'] = 'Code for reset password has sent to your email';
$lang['text_rest_reset_password'] = 'Your password have been reset, please login again';
$lang['text_rest_reset_password_invalid'] = 'Reset password code is invalid';
$lang['text_rest_reset_password_not_found'] = 'Reset password code not found';
$lang['text_rest_reset_password_expired'] = 'Link reset password has expired';
$lang['text_rest_reset_password_instruction'] = 'Reset Password Instruction';
$lang['text_rest_all_field_required'] = 'All field is required';
$lang['text_rest_success_login'] = 'Success Login';
$lang['text_rest_password_incorrect'] = 'The password you entered is incorrect';
$lang['text_rest_sent_email_hello'] = 'Hello'; 
$lang['text_rest_sent_email_paragraph_one'] = 'Someone, hopefully you, has requested to reset the password for your SMARTFIX account'; 
$lang['text_rest_sent_email_paragraph_two'] = 'if you did not perform this request, you can safely ignored this email.'; 
$lang['text_rest_sent_email_paragraph_three'] = 'Otherwise, click the link below to complete the process.'; 
$lang['text_rest_sent_email_reset_password'] = 'Reset Password';
$lang['text_rest_origin_required'] = 'Parameter origin required';
$lang['text_log_in_to_your_account'] = 'Log in to your account';
$lang['text_enter_email'] = 'Enter Email';
$lang['text_enter_password'] = 'Enter Password';
$lang['text_remember_me'] = 'Remember Me';
$lang['text_forgot_password'] = 'Forgot Password';
$lang['text_log_in'] = 'Log in';
$lang['text_login'] = 'Login';
$lang['text_logout'] = 'Logout';
$lang['text_login_required'] = 'Email & password are required';
$lang['text_nomodule'] = 'Your company is not registered on any module';

//forgot password
$lang['text_please_enter_email'] = 'Please enter your email address and follow the instructions sent to your inbox';
$lang['text_please_enter_password_reset'] = 'Please enter your new password below';
$lang['text_submit'] = 'Submit';
$lang['text_reset_password'] = 'Reset Password';
$lang['text_password_required'] = 'Password Required';
$lang['text_invalid_email_format'] = 'Invalid email format';
$lang['text_enter_password_confirm'] = 'Enter Password Confirmation';
$lang['text_password_not_match'] = 'Your new password and confirmation are not the same';

//mechanic
$lang['text_rest_undefined_email'] = 'Undefined email'; 
$lang['text_rest_undefined_model_id'] = 'Undefined model_id'; 
$lang['text_rest_undefined_block_id'] = 'Undefined block_id'; 
$lang['text_rest_undefined_company_id'] = 'Undefined Company ID'; 
$lang['text_rest_validation_result'] = 'Undefined validation result'; 
$lang['text_rest_store_data_success'] = 'Store Data Successfully'; 
$lang['text_rest_store_data_failed'] = 'Store Data Failed'; 
$lang['text_rest_success_get_model'] = 'Success Get Model'; 
$lang['text_rest_success_get_type'] = 'Success Get Type'; 
$lang['text_rest_success_get_variant'] = 'Success Get Variant'; 
$lang['text_rest_success_get_product_category'] = 'Success Get Product Category'; 
$lang['text_rest_success_get_count_of_model'] = 'Success Get Count Of Model'; 
$lang['text_rest_success_get_functional_area'] = 'Success Get Functional Areas'; 
$lang['text_rest_success_get_issue_by_functional_area'] = 'Success Get Issue by Functional Area'; 
$lang['text_rest_success_get_condition'] = 'Success Get Condition'; 
$lang['text_rest_success_get_issue_result_by_condition'] = 'Success Get Issue Result By Condition'; 
$lang['text_rest_success_get_issues'] = 'Success Get Issues'; 
$lang['text_rest_success_get_issues_directly'] = 'Success Get Issues Directly'; 
$lang['text_rest_success_get_procedures'] = 'Success Get Procedures'; 
$lang['text_rest_success_get_diagnostic_step'] = 'Success Get Diagnostic Step'; 
$lang['text_rest_success_get_repair_step'] = 'Success Get Repair Step'; 
$lang['text_rest_success_get_validation_step'] = 'Success Get Validation Step';

//users
$lang['text_rest_email_format'] = 'Incorrect E-mail Format';
$lang['text_rest_email_max_length'] = 'Email length maximum 50';
$lang['text_rest_password_required'] = 'Password is required';
$lang['text_rest_password_max_length'] = 'Password length maximum 255';
$lang['text_rest_fullname_required'] = 'Full name is required';
$lang['text_rest_fullname_length'] = 'Full name length maximum 100';
$lang['text_rest_role_required'] = 'Role is required';
$lang['text_rest_user_not_found'] = 'User Not Found';
$lang['text_rest_user_edit'] = 'User Succesfully Edited';
$lang['text_rest_user_new'] = 'New User Succesfully Created';
$lang['text_rest_user_exists'] = 'User already exists';
$lang['text_rest_action_required'] = 'Action Required';
$lang['text_rest_email_assigned_not_found'] = 'Email Not Found';
$lang['text_rest_user_deleted'] = 'User Succesfully Deleted';
$lang['text_rest_user_fail_deleted'] = 'User fail to deleted with error';
$lang['text_rest_user_cant_delete'] = 'Cannot Delete your own e-mail';
$lang['Add User'] = 'Add User';
$lang['Delete User ?'] = 'Are you sure you want to delete this user?';
$lang['Delete User'] = 'Delete User';
$lang['Enter name'] = 'Enter name';
$lang['Enter email'] = 'Enter email';
$lang['Select Role'] = 'Select Role';
$lang['Add'] = 'Add';
$lang['Edit'] = 'Edit';
$lang['Yes'] = 'Yes';
$lang['No'] = 'No';
$lang['New User'] = 'New User';

//email user
$lang['Your account is almost ready'] = 'Your account is almost ready';
$lang['To Activate your account, please click the following link :'] = 'To Activate your account, please click the following link :';
$lang['After you activate, input your password and then you can login with your registered e-mail.'] = 'After you activate, input your password and then you can login with your registered e-mail.';

//models
$lang['text_rest_model_required'] = 'Model is required';
$lang['text_rest_type_required'] = 'Type is required';
$lang['type exists'] = 'Type already exists';
$lang['text_rest_variant_required'] = 'Variant is required';
$lang['text_rest_variant_max_length'] = 'Variant length maximum 100';
$lang['text_rest_constyear_required'] = 'Const.year is required';
$lang['text_rest_constyear_max_length'] = 'Const.year length maximum 4';
$lang['text_rest_prodcat_required'] = 'Product Categories required';
$lang['text_rest_model_exists'] = 'A Model with the same name and type and construction year already exists';
$lang['text_rest_model_new'] = 'New Model Succesfully Created';
$lang['text_rest_model_edit'] = 'Model Succesfully Edited';
$lang['text_rest_model_undefined'] = 'Undefined Model ID';
$lang['text_rest_model_deleted'] = 'Model Successfully Deleted';
$lang['text_rest_model_not_found'] = 'Model Not Found';
$lang['text_rest_model_fail_deleted'] = 'Model fail to deleted with error';
$lang['text_rest_models_add'] = 'Add';
$lang['text_rest_models_button_add'] = ' Add model';
$lang['text_rest_models_confirm_delete'] = 'Are you sure you want to delete this model?';
$lang['text_rest_add_new'] = '+ add new';
$lang['text_rest_cancel'] = 'Save';

//issue
$lang['text_rest_undefined_issue'] = 'Undefined Issue ID';
$lang['text_rest_issue_not_found'] = 'Issue ID Not found';
$lang['text_rest_issue_deleted'] = 'Issue Succesfully Deleted';
$lang['text_rest_issue_fail_delete'] = 'Issue fail to deleted with error';
$lang['text_rest_issue_attached'] = 'There are other issues attached to this functional area.';
$lang['text_rest_function_deleted'] = 'Function Succesfully Deleted';
$lang['text_rest_function_fail_delete'] = 'Function fail to deleted with error';
$lang['text_rest_condition_issue_attached'] = 'There are other issues attached to this condition';
$lang['text_rest_condition_deleted'] = 'Condition Succesfully Deleted';
$lang['text_rest_condition_fail_delete'] = 'Condition fail to deleted with error';
$lang['text_rest_function_created'] = 'New Function Created';
$lang['text_rest_condition_created'] = 'New Condition Created';
$lang['text_rest_issue_created'] = 'New Issue Created';

//password
$lang['text_rest_password'] = 'Password';
$lang['text_rest_retype_password'] = 'Re-Type Password';
$lang['text_rest_retype_password_required'] = 'Re-Type Password Required';
$lang['text_rest_notmatch_password'] = 'Your password do not match.';
$lang['text_rest_confirm'] = 'Confirm';
$lang['text_rest_create_password'] = 'Create your password';
$lang['text_rest_password_expired'] = 'Invitaton code is expired';
$lang['text_rest_password_login'] = 'Your password created, please login';

//product category 
$lang['text_rest_product_category_required'] = 'Product category is required'; 
$lang['text_rest_product_category_name_exists'] = 'Product category name already exists';
$lang['text_rest_product_category_undefined'] = 'Undefined Product Category ID'; 
$lang['text_rest_product_category_deleted'] = 'Product Category Successfully Deleted'; 
$lang['text_rest_product_category_fail_deleted'] = 'Product Category fail to deleted with error';
$lang['text_rest_product_category_not_found'] = 'Product Category Not Found'; 
$lang['text_rest_product_category_unable_deleted'] = 'Unable to delete, product categories have been used in the model'; 
$lang['text_rest_product_category_new'] = 'New Product Category Succesfully Created';

$lang['text_rest_please_wait'] = 'Please wait';

//modules
$lang['Application Admin'] = 'Application Admin';
$lang['Client Admin'] = 'Client Admin';
$lang['Mechanic'] = 'Mechanic';
$lang['Shared Module'] = 'Shared Module';
$lang['Dashboard Admin'] = 'Dashboard Admin';
$lang['Companies'] = 'Companies';
$lang['Users By Admin'] = 'Users By Admin';
$lang['Dashboard Client'] = 'Dashboard Client';
$lang['Users By Client'] = 'Users By Client';
$lang['Users'] = 'Users';
$lang['Models'] = 'Models';
$lang['Issues'] = 'Issues';
$lang['Search Model/Type'] = 'Search Model/Type';
$lang['Show Procedures'] = 'Show Procedures';
$lang['Select Procedure'] = 'Select Procedure';
$lang['Validation Steps'] = 'Validation Steps';
$lang['Manage Profile'] = 'Manage Profile';
$lang['Forgot Password'] = 'Forgot Password';

// universal
$lang['Delete'] = 'Delete';
$lang['Cancel'] = 'Cancel';
$lang['+ add new'] = '+ add new';
$lang['Search...'] = 'Search...';
$lang['Success!'] = 'Success!';
$lang['Warning!'] = 'Warning!';
$lang['Error!'] = 'Error!';

//category
$lang['+ manage'] = '+ manage';
$lang['Save'] = 'Save';
$lang['Save Category'] = 'Save Category';
$lang['Add new category'] = 'Add new category';

//issue
$lang['+ Add Issue'] = 'Add Issue';
$lang['x Delete issue'] = 'Delete issue';
$lang['confirm delete issue'] = 'Are you sure you want to delete this issue?';
$lang['Issue'] = 'Issue';
$lang['Procedures'] = 'Procedures';
$lang['Funtional areas'] = 'Functional areas';
$lang['Issue id is required'] = 'Issue id is required';
$lang['Issue id length maximum 4'] = 'Issue id length maximum 4';
$lang['Procedure Description is required'] = 'Procedure Description is required';
$lang['Procedure Description length maximum 255'] = 'Procedure Description length maximum 255';
$lang['This Issue procedures already exists'] = 'This Issue procedures already exists';
$lang['New Procedure Succesfully Created'] = 'New Procedure Succesfully Created';
$lang['Procedure Succesfully Edited'] = 'Procedure Succesfully Edited';
$lang['Undefined Procedure ID'] = 'Undefined Procedure ID'; 
$lang['Procedure Successfully Deleted'] = 'Procedure Successfully Deleted'; 
$lang['Block Successfully Deleted'] = 'Block Successfully Deleted'; 
$lang['Procedure fail to deleted with error'] = 'Procedure fail to deleted with error';
$lang['Block fail to deleted with error'] = 'Block fail to deleted with error';
$lang['Procedure Not Found'] = 'Procedure Not Found';
$lang['Block Not Found'] = 'Block Not Found';
$lang['Procedure id is required'] = 'Procedure id is required';
$lang['Procedure id length maximum 11'] = 'Procedure id length maximum 11';
$lang['Block content required'] = 'Block content required';
$lang['Block content length maximum 255'] = 'Block content length maximum 255';
$lang['This Block content already exists'] = 'This Block content already exists';
$lang['Procedure Block Succesfully Created'] = 'Procedure Block Succesfully Created';
$lang['Procedure Block Succesfully Edited'] = 'Procedure Block Succesfully Edited';

$lang['Functional area'] = 'Functional Area';
$lang['Attach to model /type(s)'] = 'Attach to model /type(s)';
$lang['Validate Command'] = 'Validate if indicator comes on again.';
$lang['Condition'] = 'Condition';
$lang['Save Changes'] = 'Save Changes';
$lang['Create first Procedure'] = 'Create first Procedure';
$lang['+ Add Procedures'] = '+ Add Procedures';
$lang['Enter new procedure'] = 'Enter new procedure';
$lang['Fault code(s)'] = 'FAULT CODE(S):';
$lang['Create procedure command'] = 'Once you have created an issue, you can then add procedures to that issue, every procedure can have one or more diagnostic steps and every diagnostic step should have a repair step. ';
$lang['unable delete functional area'] = 'Unable to delete, functional area have been used in the issue functional area';
$lang['text_rest_functional_area_new'] = 'New Functional Area Succesfully Created';
$lang['text_rest_condition_new'] = 'New Condition Succesfully Created';
$lang['undefined functional area id'] = 'Undefined Functional Area ID';
$lang['functional area deleted'] = 'Functional Area Successfully Deleted'; 
$lang['functional area fail deleted'] = 'Functional Area fail to deleted with error';
$lang['functional area unable deleted'] = 'Unable to delete, functional area have been used in the issue functional area';
$lang['functional area not found'] = 'Functional Area Not Found';  
$lang['functional area exist'] = 'Functional Area already exists';  
$lang['undefined condition id'] = 'Undefined Condition ID';
$lang['condition deleted'] = 'Condition Successfully Deleted'; 
$lang['condition fail deleted'] = 'Condition fail to deleted with error';
$lang['condition unable deleted'] = 'Unable to delete, condition have been used in the issue condition';
$lang['condition not found'] = 'Condition Not Found'; 
$lang['condition exist'] = 'Condition already exists'; 

/* 
    Added By Eri 
    Description : Lang for issue
    Modified Date   : 28-06-2019
    Lang English
*/
$lang['Issue name is required'] = 'Issue name is required';
$lang['Fault codes is required'] = 'Fault codes is required';
$lang['Model is required'] = 'Model is required';
$lang['Model name is required'] = 'Model name is required';
$lang['Model name exist'] = 'Model name already exists';
$lang['Function area is required'] = 'Functional area is required';
$lang['Condition is required'] = 'Condition is required';
$lang['Issue id is required'] = 'Issue id is required';
$lang['Validate step is required'] = 'Validate step is required';

$lang['Issue Option'] = 'Options';
$lang['Edit Issue'] = 'Edit';

$lang['text_rest_function_created_error'] = "New Function Error Created";
$lang['text_rest_function_edited'] = 'Function Succesfully Updated';
$lang['text_rest_function_edited_error'] = 'Function Unsuccesfully Updated';
$lang['text_rest_condition_created_error'] = 'New Condition Error Created';
$lang['text_rest_condition_edited'] = 'Condition Succesfully Updated';
$lang['text_rest_condition_edited_error'] = 'Condition Unsuccesfully Updated';

$lang['Issue Succesfully Edited'] = 'Issue Succesfully Edited';

$lang['txt_lang_star_flag_success'] = 'Priority successfully updated';
$lang['txt_lang_star_flag_error'] = 'Priority Unsuccesfully Updated';

$lang['datatable_no_data']  = 'No matching records found';
$lang['There are other issues using this model'] = 'This model is attached to one or more issues. Deselect this model first from the issues, before deleting this model';

/* Confirm delete procedure & block */
$lang['confrm_delete_block']    = 'Are you sure you want to delete this block?';
$lang['confrm_delete_procedure']    = 'Are you sure you want to delete this procedures?';

$lang['User cannot downgrade user role']    = 'User cannot downgrade user role from Client administrator to Mechanic!';

$lang['add_block']  = 'Add Block';
$lang['edit_block_diagnostic']  = 'Edit block diagnostic';
$lang['edit_block_repair']  = 'Edit blok repair';

$lang['Your Account has been deleted!']  = 'This Account has been deleted!';

$lang['Block content image'] = 'Cant input image';

/* Warning moved drag n drop */
$lang['Block cannot moved'] = 'Block cannot moved';
$lang['Repair cannot moved'] = 'Repair cannot moved';

$lang['next'] = 'next';
