<?php

/*
 * Dutch language
 */

$lang['title'] = 'Smartfix Dutch';
$lang['text_rest_invalid_api_key'] = 'Invalid API key %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_ip_address_time_limit'] = 'This IP Address has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';

$lang['text_rest_unregistered_user'] = 'Niet-geregistreerde gebruiker';
$lang['text_rest_session_expired'] = 'Sessie verlopen';
$lang['text_rest_enter_email_login'] = 'Vul je e-mail adres in';
$lang['text_rest_enter_password_login'] = 'Vul je wachtwoord in';
$lang['text_rest_forgot_password_login'] = 'Wachtwoord vergeten?';
$lang['text_rest_search_models_search_models'] = 'Zoek modellen';
$lang['text_rest_functional_areas_selected_model'] = 'Functionele gebieden';
$lang['text_rest_conditions_selected_model'] = 'Condities';
$lang['text_rest_issues_issues_switch'] = 'Complicaties';
$lang['text_rest_apply_conditions'] = 'Toepassen';
$lang['text_rest_back_to_functional_areas_issues'] = 'Terug naar functionele gebieden';
$lang['text_rest_back_to_issues_procedures'] = 'Terug naar complicaties';
$lang['text_rest_procedures_procedures'] = 'Procedures';
$lang['text_rest_times_solved_procedures'] = 'Opgelost';
$lang['text_rest_back_to_procedures'] = 'Terug naar procedures';
$lang['text_rest_diagnostic_step'] = 'Diagnose stap';
$lang['text_rest_yes_repair'] = 'Ja, ga naar reparatie stap';
$lang['text_rest_no_repair'] = 'Nee, ga naar volgende diagnose stap';
$lang['text_rest_back_diagnostic'] = 'Terug naar diagnose stap';
$lang['text_rest_repair_step'] = 'Reparatie stap';
$lang['text_rest_validation_repair_step'] = 'Validatie stap';
$lang['text_rest_back_repair'] = 'Terug naar reparatie stap';
$lang['text_rest_validation_step_validation_step'] = 'Validatie stap';
$lang['text_rest_not_solved_validation_step'] = 'Niet opgelost';
$lang['text_rest_solved_validation_step'] = 'Opgelost';
$lang['text_rest_issue_solved_solved'] = 'Complicatie opgelost';
$lang['text_rest_back_home_solved'] = 'Terug naar startscherm';
$lang['text_rest_issue_not_solved'] = 'Complicatie niet opgelost';
$lang['text_rest_back_procedures'] = 'Terug naar procedures';
$lang['text_rest_users_side_menu'] = 'Gebruikers';
$lang['text_rest_models_side_menu'] = 'Modellen';
$lang['text_rest_issues_side_menu'] = 'Complicaties';
$lang['text_rest_profile_side_menu'] = 'Profiel';
$lang['text_rest_name_users'] = 'Naam';
$lang['text_rest_email_users'] = 'E-mail';
$lang['text_rest_role_users'] = 'Rol';
$lang['text_rest_invited_users'] = 'Uitgenodigd';
$lang['text_rest_options_users'] = 'Opties';
$lang['text_rest_yes_users'] = 'Ja';
$lang['text_rest_no_users'] = 'Nee';
$lang['text_rest_edit_users'] = 'Aanpassen';
$lang['text_rest_delete_users'] = 'Verwijderen';
$lang['text_rest_search_users'] = 'Zoeken...';
$lang['text_rest_model_models'] = 'Model';
$lang['text_rest_type_models'] = 'Type';
$lang['text_rest_constr_year'] = 'Bouwjaar';
$lang['text_rest_product_category'] = 'Product categorie';
$lang['text_rest_options_models'] = 'Opties';
$lang['text_rest_variant'] = 'Variant';
$lang['text_rest_edit_models'] = 'Aanpassen';
$lang['text_rest_delete_models'] = 'Model verwijderen';
$lang['text_rest_search_models'] = 'Zoeken...';
$lang['text_rest_issues_issues'] = 'Complicaties';
$lang['text_rest_issue_issues'] = 'Complicatie';
$lang['text_rest_procedures_issues'] = 'Procedures';
$lang['text_rest_functional_areas_issues'] = 'Hoofdfuncties';
$lang['text_rest_models_issues'] = 'Modellen';
$lang['text_rest_add_issue_issues'] = 'Complicatie toevoegen';
$lang['text_rest_search_issues'] = 'Zoeken';
$lang['text_rest_create_issue'] = 'Nadat u een complicatie heeft aangemaakt, kunt u vervolgens ';
$lang['text_rest_attach_issue'] = 'Voeg modellen toe';
$lang['text_rest_selected_issue'] = 'geselecteerd';
$lang['text_rest_insert_issue'] = 'Opslaan';
$lang['text_rest_manage_issue'] = 'Beheren';
$lang['text_rest_add_procedure'] = 'Procedure toevoegen';
$lang['text_rest_add_block_procedure'] = 'Blok toevoegen';
$lang['text_rest_type_diagnostic_procedure'] = 'Type: Diagnose stap';
$lang['text_rest_type_repair_procedure'] = 'Type: Reparatie stap';
$lang['text_rest_delete_procedure'] = 'Verwijderen';

//companies
$lang['text_rest_user_success'] = 'Succes';
$lang['text_rest_company_name_required'] = 'Bedrijfsnaam is verplicht';
$lang['text_rest_company_max_length'] = 'Bedrijfsnaam lengte maximaal 100';
$lang['text_rest_interface_lang_max_length'] = 'Interface Taallengte maximaal 5';
$lang['text_rest_company'] = 'Bedrijf';
$lang['text_rest_exists'] = 'bestaat al';
$lang['text_rest_company_new'] = 'Nieuw bedrijf met succes gemaakt';
$lang['text_rest_company_edit'] = 'Bedrijf succesvol bewerkt';
$lang['text_rest_company_undefined'] = 'Niet gedefinieerd bedrijf';
$lang['text_rest_company_not_found'] = 'Bedrijfs-ID Niet gevonden';
$lang['text_rest_company_deleted'] = 'Bedrijf succesvol verwijderd';
$lang['text_rest_company_fail_deleted'] = 'Bedrijf kan niet met fouten worden verwijderd';

//login
$lang['text_rest_email_required'] = 'E-mail is verplicht';
$lang['text_rest_email_found'] = 'E-mail gevonden';
$lang['text_rest_email_not_found'] = 'E-mail niet gevonden';
$lang['text_rest_email_not_register'] = 'De e-mail die je hebt ingevoerd, is niet geregistreerd';
$lang['text_rest_reset_password_sent'] = 'Code voor reset-wachtwoord is verzonden naar uw e-mailadres';
$lang['text_rest_reset_password'] = 'Uw wachtwoord is opnieuw ingesteld, log opnieuw in';
$lang['text_rest_reset_password_invalid'] = 'Reset wachtwoordcode is ongeldig';
$lang['text_rest_reset_password_not_found'] = 'Reset wachtwoordcode niet gevonden';
$lang['text_rest_reset_password_expired'] = 'Het wachtwoord voor het opnieuw instellen van de link is verlopen';
$lang['text_rest_reset_password_instruction'] = 'Reset wachtwoord Instruction';
$lang['text_rest_all_field_required'] = 'Alle velden zijn verplicht';
$lang['text_rest_success_login'] = 'Succes Inloggen';
$lang['text_rest_password_incorrect'] = 'Het wachtwoord dat je hebt ingevoerd is onjuist';
$lang['text_rest_sent_email_hello'] = 'Hallo'; 
$lang['text_rest_sent_email_paragraph_one'] = 'Iemand, hopelijk u, heeft gevraagd om het wachtwoord voor uw SMARTFIX-account opnieuw in te stellen'; 
$lang['text_rest_sent_email_paragraph_two'] = 'als u dit verzoek niet hebt uitgevoerd, kunt u deze e-mail veilig negeren.'; 
$lang['text_rest_sent_email_paragraph_three'] = 'Klik anders op de onderstaande link om het proces te voltooien.'; 
$lang['text_rest_sent_email_reset_password'] = 'Reset wachtwoord';
$lang['text_rest_origin_required'] = 'Parameter oorsprong vereist';
$lang['text_log_in_to_your_account'] = 'Log in op jouw account';
$lang['text_enter_email'] = 'Voer email in';
$lang['text_enter_password'] = 'Voer wachtwoord in';
$lang['text_remember_me'] = 'Onthoud mij';
$lang['text_forgot_password'] = 'Wachtwoord vergeten';
$lang['text_log_in'] = 'Log in';
$lang['text_login'] = 'Login';
$lang['text_logout'] = 'Logout';
$lang['text_login_required'] = 'E-mail en wachtwoord zijn verplicht';
$lang['text_nomodule'] = 'Uw bedrijf is niet geregistreerd op een module';

//forgot password
$lang['text_please_enter_email'] = 'Voer uw e-mailadres in en volg de instructies die naar uw inbox zijn verzonden';
$lang['text_please_enter_password_reset'] = 'Voer hieronder uw nieuwe wachtwoord in';
$lang['text_submit'] = 'Voorleggen';
$lang['text_reset_password'] = 'Reset wachtwoord';
$lang['text_password_required'] = 'Wachtwoord benodigd';
$lang['text_invalid_email_format'] = 'Ongeldige email formaat';
$lang['text_enter_password_confirm'] = 'Voer wachtwoordbevestiging in';
$lang['text_password_not_match'] = 'Uw nieuwe wachtwoord en bevestiging zijn niet hetzelfde';

//mechanic
$lang['text_rest_undefined_email'] = 'Niet-gedefinieerde e-mail'; 
$lang['text_rest_undefined_model_id'] = 'Niet-gedefinieerde model_id'; 
$lang['text_rest_undefined_block_id'] = 'Niet-gedefinieerde blok_id'; 
$lang['text_rest_undefined_company_id'] = 'Ongedefinieerde bedrijfs-ID'; 
$lang['text_rest_validation_result'] = 'Ongedefinieerd validatieresultaat'; 
$lang['text_rest_store_data_success'] = 'Gegevens met succes opslaan'; 
$lang['text_rest_store_data_failed'] = 'Winkelgegevens mislukt';
$lang['text_rest_success_get_model'] = 'Succes Model krijgen'; 
$lang['text_rest_success_get_type'] = 'Succes Get Type'; 
$lang['text_rest_success_get_variant'] = 'Succes Get Variant'; 
$lang['text_rest_success_get_product_category'] = 'Succes Productcategorie ophalen'; 
$lang['text_rest_success_get_count_of_model'] = 'Succes Get Count Of Model'; 
$lang['text_rest_success_get_functional_area'] = 'Succes Krijg functionele gebieden'; 
$lang['text_rest_success_get_issue_by_functional_area'] = 'Succes Get Issue by Functional Area'; 
$lang['text_rest_success_get_condition'] = 'Succes krijgt voorwaarde'; 
$lang['text_rest_success_get_issue_result_by_condition'] = 'Succes Get Issue Result By Condition'; 
$lang['text_rest_success_get_issues'] = 'Succes Krijg problemen'; 
$lang['text_rest_success_get_issues_directly'] = 'Succes Krijg problemen direct'; 
$lang['text_rest_success_get_procedures'] = 'Succes Get Procedures'; 
$lang['text_rest_success_get_diagnostic_step'] = 'Succes Get Diagnostic Step'; 
$lang['text_rest_success_get_repair_step'] = 'Succes Get Repair Step'; 
$lang['text_rest_success_get_validation_step'] = 'Succes Get Validation Step';

//users
$lang['text_rest_email_format'] = 'Onjuiste e-mailindeling';
$lang['text_rest_email_max_length'] = 'E-mail lengte maximaal 50';
$lang['text_rest_password_required'] = 'Een wachtwoord is verplicht';
$lang['text_rest_password_max_length'] = 'Wachtwoord lengte maximaal 255';
$lang['text_rest_fullname_required'] = 'Volledige naam is verplicht';
$lang['text_rest_fullname_length'] = 'Volledige naamlengte maximaal 100';
$lang['text_rest_role_required'] = 'De rol is vereist';
$lang['text_rest_user_not_found'] = 'Gebruiker niet gevonden';
$lang['text_rest_user_edit'] = 'Gebruiker succesvol bewerkt';
$lang['text_rest_user_new'] = 'Nieuwe gebruiker succesvol gemaakt';
$lang['text_rest_user_exists'] = 'Gebruiker bestaat al';
$lang['text_rest_action_required'] = 'Actie vereist';
$lang['text_rest_email_assigned_not_found'] = 'email niet gevonden';
$lang['text_rest_user_deleted'] = 'Gebruiker succesvol verwijderd';
$lang['text_rest_user_fail_deleted'] = 'Gebruiker kan niet met fouten worden verwijderd';
$lang['text_rest_user_cant_delete'] = 'Kan uw eigen e-mail niet verwijderen';
$lang['Add User'] = 'Voeg gebruiker toe';
$lang['Delete User ?'] = 'Weet je zeker dat je dit gebruiker wilt verwijderen?';
$lang['Delete User'] = 'Verwijder gebruiker';
$lang['Enter name'] = 'Voer naam in';
$lang['Enter email'] = 'Voer email in';
$lang['Select Role'] = 'Selecteer Rol';
$lang['Add'] = 'Toevoegen';
$lang['Edit'] = 'Bewerk';
$lang['Yes'] = 'Ja';
$lang['No'] = 'Nee';
$lang['New User'] = 'Nieuwe gebruiker';

//email user
$lang['Your account is almost ready'] = 'Je account is bijna klaar';
$lang['To Activate your account, please click the following link :'] = 'Klik op de volgende link om uw account te activeren:';
$lang['After you activate, input your password and then you can login with your registered e-mail.'] = 'Nadat je hebt geactiveerd, voer je je wachtwoord in en dan kun je inloggen met je geregistreerde e-mail.';



//models
$lang['text_rest_model_required'] = 'Model is verplicht';
$lang['text_rest_type_required'] = 'Type is verplicht';
$lang['type exists'] = 'Type bestaat al';
$lang['text_rest_variant_required'] = 'Variant is verplicht';
$lang['text_rest_variant_max_length'] = 'Variant lengte maximaal 100';
$lang['text_rest_constyear_required'] = 'Bouwjaar is verplicht';
$lang['text_rest_constyear_max_length'] = 'Bouwjaar lengte maximaal 4';
$lang['text_rest_prodcat_required'] = 'Product categorieën vereist';
$lang['text_rest_model_exists'] = 'Model met dezelfde naam en type en bouwjaar bestaat al';
$lang['text_rest_model_new'] = 'Nieuw model succesvol gemaakt';
$lang['text_rest_model_edit'] = 'Model met succes bewerkt';
$lang['text_rest_model_undefined'] = 'Ongedefinieerde model-ID';
$lang['text_rest_model_deleted'] = 'Model is succesvol verwijderd';
$lang['text_rest_model_not_found'] = 'Model niet gevonden';
$lang['text_rest_model_fail_deleted'] = 'Model kan niet worden verwijderd met een fout';
$lang['text_rest_models_add'] = 'Toevoegen';
$lang['text_rest_models_button_add'] = ' Voeg een model toe.';
$lang['text_rest_models_confirm_delete'] = 'Weet je zeker dat je dit model wilt verwijderen?';
$lang['text_rest_add_new'] = '+ voeg nieuw toe';
$lang['text_rest_cancel'] = 'Opslaan';

//issue
$lang['text_rest_undefined_issue'] = 'Niet gedefinieerde Issue ID';
$lang['text_rest_issue_not_found'] = 'Probleem-ID Niet gevonden';
$lang['text_rest_issue_deleted'] = 'Probleem is verwijderd';
$lang['text_rest_issue_fail_delete'] = 'Issue fail to deleted with error';
$lang['text_rest_issue_attached'] = 'Er zijn andere problemen verbonden aan dit functionele gebied.';
$lang['text_rest_function_deleted'] = 'Functie succesvol verwijderd';
$lang['text_rest_function_fail_delete'] = 'Functie kan niet worden verwijderd met een fout';
$lang['text_rest_condition_issue_attached'] = 'Er zijn nog andere problemen aan deze voorwaarde verbonden';
$lang['text_rest_condition_deleted'] = 'Staat met succes verwijderd';
$lang['text_rest_condition_fail_delete'] = 'Staat kan niet worden verwijderd met een fout';
$lang['text_rest_function_created'] = 'Nieuwe functie gemaakt';
$lang['text_rest_condition_created'] = 'Nieuwe voorwaarde gemaakt';
$lang['text_rest_issue_created'] = 'Nieuw nummer aangemaakt';


//password
$lang['text_rest_password'] = 'Wachtwoord';
$lang['text_rest_retype_password'] = 'Overtypen Wachtwoord';
$lang['text_rest_retype_password_required'] = 'Type het wachtwoord opnieuw in';
$lang['text_rest_notmatch_password'] = 'Uw wachtwoord en bevestigingswachtwoord komen niet overeen.';
$lang['text_rest_confirm'] = 'Bevestigen';
$lang['text_rest_create_password'] = 'Maak uw wachtwoord aan';
$lang['text_rest_password_expired'] = 'Uitnodigingscode is verlopen';
$lang['text_rest_password_login'] = 'Uw wachtwoord is aangemaakt, log alstublieft in';

//product category 
$lang['text_rest_product_category_required'] = 'Product categorie is vereist'; 
$lang['text_rest_product_category_name_exists'] = 'De naam van de productcategorie bestaat al';
$lang['text_rest_product_category_undefined'] = 'Ongedefinieerde productcategorie-ID'; 
$lang['text_rest_product_category_deleted'] = 'Productcategorie succesvol verwijderd'; 
$lang['text_rest_product_category_fail_deleted'] = 'Productcategorie kan niet met fouten worden verwijderd'; 
$lang['text_rest_product_category_not_found'] = 'Productcategorie niet gevonden'; 
$lang['text_rest_product_category_unable_deleted'] = 'Kan niet verwijderen, productcategorieën zijn in het model gebruikt'; 
$lang['text_rest_product_category_new'] = 'Nieuwe productcategorie met succes gemaakt';

$lang['text_rest_please_wait'] = 'Even geduld aub';

//modules
$lang['Application Admin'] = 'Application Admin';
$lang['Client Admin'] = 'Clientbeheerder';
$lang['Mechanic'] = 'Monteur';
$lang['Shared Module'] = 'Gedeelde module';
$lang['Dashboard Admin'] = 'Dashboard-beheerder';
$lang['Companies'] = 'Bedrijven';
$lang['Users By Admin'] = 'Gebruikers door Admin';
$lang['Dashboard Client'] = 'Dashboard Client';
$lang['Users By Client'] = 'Gebruikers per klant';
$lang['Users'] = 'Gebruikers';
$lang['Models'] = 'Modellen';
$lang['Issues'] = 'Problemen';
$lang['Search Model/Type'] = 'Zoek model / type';
$lang['Show Procedures'] = 'Procedures weergeven';
$lang['Select Procedure'] = 'Selecteer Procedure';
$lang['Validation Steps'] = 'Validatiestappen';
$lang['Manage Profile'] = 'Beheer profiel';
$lang['Forgot Password'] = 'Wachtwoord vergeten';

// universal
$lang['Delete'] = 'Verwijderen';
$lang['Cancel'] = 'Annuleren';
$lang['+ add new'] = '+ voeg nieuw toe';
$lang['Search...'] = 'Zoeken...';
$lang['Success!'] = 'Succes!';
$lang['Warning!'] = 'Waarschuwing!';
$lang['Error!'] = 'Fout!';

//category
$lang['+ manage'] = '+ beheren';
$lang['Save'] = 'Opslaan';
$lang['Save Category'] = 'Bewaar Categorie';
$lang['Add new category'] = 'Voeg een nieuwe categorie toe';

//issue
$lang['+ Add Issue'] = 'Voeg een probleem toe.';
$lang['x Delete issue'] = 'Verwijder probleem';
$lang['confirm delete issue'] = 'Weet je zeker dat je dit probleem wilt verwijderen?';
$lang['Issue'] = 'Probleem';
$lang['Procedures'] = 'Procedures';
$lang['Funtional areas'] = 'Functionele gebieden';
$lang['Issue id is required'] = 'Issue id is verplicht';
$lang['Issue id length maximum 4'] = 'Issue id lengte maximaal 4';
$lang['Procedure Description is required'] = 'Procedure Beschrijving is vereist';
$lang['Procedure Description length maximum 255'] = 'Procedure Beschrijving lengte maximaal 255';
$lang['This Issue procedures already exists'] = 'Deze Issue-procedures bestaan al';
$lang['New Procedure Succesfully Created'] = 'Nieuwe procedure succesvol gemaakt';
$lang['Procedure Succesfully Edited'] = 'Procedure succesvol bewerkt';
$lang['Undefined Procedure ID'] = 'Ongedefinieerde procedure-ID';
$lang['Procedure Successfully Deleted'] = 'Procedure succesvol verwijderd';  
$lang['Block Successfully Deleted'] = 'Blokkeren succesvol verwijderd'; 
$lang['Procedure fail to deleted with error'] = 'Procedure kan niet worden verwijderd met een fout';
$lang['Block fail to deleted with error'] = 'Blokkeren kan niet worden verwijderd met een fout';
$lang['Procedure Not Found'] = 'Procedure niet gevonden';
$lang['Block Not Found'] = 'Blokkeren niet gevonden';
$lang['Procedure id is required'] = 'Procedure-ID is verplicht';
$lang['Procedure id length maximum 11'] = 'Procedure id lengte maximaal 11';
$lang['Block content required'] = 'De vereiste inhoud blokkeren';
$lang['Block content length maximum 255'] = 'Blokkeer inhoudslengte maximaal 255';
$lang['This Block content already exists'] = 'Deze Block-inhoud bestaat al';
$lang['Procedure Block Succesfully Created'] = 'Procedure Blokkeren succesvol aangemaakt';
$lang['Procedure Block Succesfully Edited'] = 'Procedure Blokkeren succesvol bewerkt';

$lang['Functional area'] = 'Functioneel Gebied';
$lang['Attach to model /type(s)'] = 'Bevestig aan model / type (s)';
$lang['Validate Command'] = 'Valideer of de indicator weer gaat branden.';
$lang['Condition'] = 'Staat';
$lang['Save Changes'] = 'Wijzigingen Opslaan';
$lang['Create first Procedure'] = 'Maak eerste procedure aan';
$lang['+ Add Procedures'] = '+ Procedures Toevoegen';
$lang['Enter new procedure'] = 'Voer een nieuwe procedure in';
$lang['Fault code(s)'] = 'FOUTCODE(S):';
$lang['Create procedure command'] = 'Nadat u een probleem hebt gecreëerd, kunt u vervolgens procedures aan dat probleem toevoegen, kan elke procedure een of meer diagnostische stappen hebben en moet elke diagnostische stap een reparatiestap bevatten.';
$lang['unable delete functional area'] = 'Niet in staat om te verwijderen, functioneel gebied is gebruikt in het functionele gebied van het probleem';
$lang['text_rest_functional_area_new'] = 'Nieuw functioneel gebied met succes gemaakt'; 
$lang['text_rest_condition_new'] = 'Nieuwe voorwaarde met succes gemaakt';
$lang['undefined functional area id'] = 'Niet-gedefinieerde functionele gebied-ID';
$lang['functional area deleted'] = 'Functioneel gebied is succesvol verwijderd';
$lang['functional area fail deleted'] = 'Functioneel gebied kan niet worden verwijderd met een fout';
$lang['functional area unable deleted'] = 'Niet in staat om te verwijderen, functioneel gebied is gebruikt in het functionele gebied van het probleem'; 
$lang['functional area not found'] = 'Functioneel gebied niet gevonden'; 
$lang['functional area exist'] = 'Functional Area bestaan al'; 
$lang['undefined condition id'] = 'Niet-gedefinieerde voorwaarde-ID';
$lang['condition deleted'] = 'Conditie succesvol verwijderd';
$lang['condition fail deleted'] = 'Staat kan niet worden verwijderd met erro';
$lang['condition unable deleted'] = 'Kan niet verwijderen, voorwaarde is gebruikt in de uitgavevoorwaarde'; 
$lang['condition not found'] = 'Conditie niet gevonden'; 
$lang['condition exist'] = 'Condition bestaat al'; 

/* 
    Added By Eri 
    Description : Lang for issue
    Modified Date   : 28-06-2019
    Lang Dutch

*/
$lang['Issue name is required'] = 'Probleemnaam is vereist';
$lang['Fault codes is required'] = 'Foutcodes zijn vereist';
$lang['Model is required'] = 'Model is verplicht';
$lang['Model name is required'] = 'Modelnaam is verplicht';
$lang['Model name exist'] = 'Modelnaam bestaat al';
$lang['Function area is required'] = 'Functioneel gebied is vereist';
$lang['Condition is required'] = 'Voorwaarde is vereist';
$lang['Issue id is required'] = 'Issue id is verplich';
$lang['Validate step is required'] = 'Valideren stap is vereist';

$lang['Issue Option'] = 'Opties';
$lang['Edit Issue'] = 'Aanpassen';

$lang['text_rest_function_created_error'] = "Nieuwe functie Fout gemaakt";
$lang['text_rest_function_edited'] = 'Functie succesvol bijgewerkt';
$lang['text_rest_function_edited_error'] = 'Functie Niet succesvol bijgewerkt';
$lang['text_rest_condition_created_error'] = 'Nieuwe conditiefout aangemaakt';
$lang['text_rest_condition_edited'] = 'Conditie succesvol bijgewerkt';
$lang['text_rest_condition_edited_error'] = 'Conditie Mislukt bijgewerkt';

$lang['Issue Succesfully Edited'] = 'Probleem succesvol bewerkt';

/* Add By Eri 
   Date : 2019-07-04
   Description : Manage Procedure
*/
$lang['txt_lang_star_flag_success'] = 'Prioriteit succesvol bijgewerkt';
$lang['txt_lang_star_flag_error'] = 'Prioriteit niet succesvol bijgewerkt';

$lang['datatable_no_data']  = 'Geen overeenkomend records gevonden';
$lang['There are other issues using this model'] = 'Dit model is gekoppeld aan een of meer problemen. Deselecteer dit model eerst uit de problemen, voordat u dit model verwijdert';

/* Confirm delete procedure & block */
$lang['confrm_delete_block']    = 'Weet je zeker dat je dit blok wilt verwijderen?';
$lang['confrm_delete_procedure']    = 'Weet u zeker dat u deze procedures wilt verwijderen?';

$lang['User cannot downgrade user role'] = 'Gebruiker kan gebruikersrol niet downgraden van Client administrator naar Mechanic!';

$lang['add_block']  = 'Adblock';
$lang['edit_block_diagnostic']  = 'Blok diagnostisch Bewerken';
$lang['edit_block_repair']  = 'Bewerk blok reparatie';

$lang['Your Account has been deleted!']  = 'Dit account is verwijderd';

$lang['Block content image'] = 'Kan geen beeldformaat invoeren';

/* Warning moved drag n drop */
$lang['Block cannot moved'] = 'Blok kan niet worden verplaatst';
$lang['Repair cannot moved'] = 'Reparatie kan niet worden verplaatst';

$lang['next'] = 'volgende';