<?php
defined('BASEPATH') OR exit('No direct script access allowed');


    
/* Created : safari.erie@gmail.com
 * Created_dt : 2019122-
 *
 * TODO
 * Login user with param username and password
 * retrieve all menu auth role login user

 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20191221    Starting Class API Type Reward
 * 2      20191221    Create inq all master type reward
 * 3      20191221    Insert and update data master type reward 
 * */

// load library REST Service
//require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/REST_Controller.php';

class API_Type_Reward extends REST_Controller {
    // constructor to
	function __construct() 
    {
		parent:: __construct();
		// load class model user_model
		$this->load->model('type_reward_model','tr');
    }

    function index_get(){
        $action = $this->input->get('action');

        if($action != '' || $action != null){
            if($action == 'getAll'){
                $data = $this->tr->getAllTypeReward();
                $result = [
                    'status' => true,
                    'message' => '',
                    'data' => $data
                ];
                $this->set_response($result, REST_Controller::HTTP_OK);
            }
        }
    }

    function index_post(){
        $action = $this->input->post('action');
        $name = $this->input->post('name');
        $score_reward = $this->input->post('score_reward');
        $type_reward_id = $this->input->post('type_reward_id');
        $created_by = $this->input->post('created_by');
        $changed_by = $this->input->post('changed_by');

        if($action != null || $action == ''){
            if($action == 'new'){
                if($name == '' || $name == null){
                    $result = [
						'status' => false,
						'message' => 'name required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }else if($score_reward == '' || $score_reward == null){
                    $result = [
						'status' => false,
						'message' => 'score required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }else{
                    $data = array(
                        'name'  => $name,
                        'score_reward'  => $score_reward,
                        'created_by'    => $created_by,
                        'created_dt'    => date('Y-m-d')
                    );

                    $data = $this->tr->insert_update($data,'');
                    $result = [
                        'status' => $data,
                        'message' => 'Successfull insert data',
                        'data' => array()
                    ];
                    $this->set_response($result, REST_Controller::HTTP_OK);
                }
            }else if($action == 'update'){
               if($type_reward_id == null || $type_reward_id == null){
                $result = [
                    'status' => false,
                    'message' => 'Reward id required',
                    'data' => array()
                ];
                $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
               }else{
                   $data = $this->tr->getTypeRewardId($type_reward_id);
                   if(count($data) > 0 && $data != false){
                        $data = array(
                            'name'  => $name,
                            'score_reward'  => $score_reward,
                            'created_by'    => $created_by,
                            'created_dt'    => date('Y-m-d')
                        );

                        $data = $this->tr->insert_update($data,$type_reward_id);
                        $result = [
                            'status' => $data,
                            'message' => 'Successfull update data',
                            'data' => array()
                        ];
                        $this->set_response($result, REST_Controller::HTTP_OK);
                   }else{
                        $result = [
                            'status' => false,
                            'message' => 'Reward id Not Found',
                            'data' => array()
                        ];
                        $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                   }
               }
            }
        }

    }



}