<?php
defined('BASEPATH') OR exit('No direct script access allowed');


    
/* Created : safari.erie@gmail.com
 * Created_dt : 2019122-
 *
 * TODO
 * Login user with param username and password
 * retrieve all menu auth role login user

 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20191222    Starting Class API Transaction
 * */

// load library REST Service
//require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/REST_Controller.php';

class API_transaction extends REST_Controller {

    function __construct() 
    {
		parent:: __construct();
		// load class model user_model
		$this->load->model('master_product_model','mpm');
		$this->load->model('type_reward_model','tr');
        $this->load->model('users_model','um');
        $this->load->model('transaction_model','tm');
    }

    function index_post(){

        $action = $this->input->post('action');
        $user_id = $this->input->post('user_id');
        $product_id = $this->input->post('product_id');
        if($action != ''){
           if($action == 'buy'){
                $check_user_id = $this->um->get_user_by_id($user_id);
                if(count($check_user_id) > 0 && $user_id != false ){
                    if($check_user_id->user_group_id == 2){
                            // lanjut
                        $data = array(
                            'product_id'    => $product_id,
                            'user_id'       => $user_id
                        );
                        $data = $this->tm->buy_product($data);
                        $result = [
                            'status' => $data,
                            'message' => 'Successfull Transaction data',
                            'data' => array()
                        ];
                        $this->set_response($result, REST_Controller::HTTP_OK);
                    }else{
                        $result = [
                            'status' => false,
                            'message' => 'dont have authorization'
                        ];
                        $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                    }
                }else{
                    $result = [
                        'status' => false,
                        'message' => 'User Id is required'
                    ];
                    $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }
           }
        }else{
            $result = [
                'status' => false,
                'message' => 'param action is required'
            ];
            $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    function index_get(){

        $action = $this->input->get('action');
        $user_id = $this->input->get('user_id');
        if($action != ''){
            if($action == "get_all_trans"){
                $check_user_id = $this->um->get_user_by_id($user_id);
                
                if(count($check_user_id) > 0 && $user_id != false ){
                    if($check_user_id->user_group_id == 1){
                        $data = $this->tm->get_transaction('');
                        
                        $result = [
                            'status' => true,
                            'message' => 'Retrieve All Transaction',
                            'data' => $data
                        ];
                        $this->set_response($result, REST_Controller::HTTP_OK);
                    }else{
                        $result = [
                            'status' => false,
                            'message' => 'dont have authorization'
                        ];
                        $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                    }
                }else{
                    $result = [
                        'status' => false,
                        'message' => 'User id not found'
                    ];
                    $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }
            }else if($action == 'get_user_trans'){
                
                $check_user_id = $this->um->get_user_by_id($user_id);              
                if($check_user_id != false ){
                   if($check_user_id->user_group_id == 2){
                        $data = $this->tm->get_transaction($user_id);
                        $result = [
                            'status' => true,
                            'message' => 'Retrieve Transaction By Id',
                            'data' => $data,
                        ];
                        $this->set_response($result, REST_Controller::HTTP_OK);
                    
                       
                   }else{
                        $result = [
                            'status' => false,
                            'message' => 'Dont have authorization'
                        ];
                        $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                   }
                   
                }else{
                    $result = [
                        'status' => false,
                        'message' => 'User id not found'
                    ];
                    $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }else{
            $result = [
                'status' => false,
                'message' => 'param action is required'
            ];
            $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}
