<?php
defined('BASEPATH') OR exit('No direct script access allowed');


    
/* Created : safari.erie@gmail.com
 * Created_dt : 2019122-
 *
 * TODO
 * Login user with param username and password
 * retrieve all menu auth role login user

 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20191221    Starting Class API Type Master Product
 * 2      20191221    Create inq all data master product
 * 3      20191221    Create insert and update data
 * */

// load library REST Service
//require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/REST_Controller.php';

class API_M_Product extends REST_Controller {

    function __construct() 
    {
		parent:: __construct();
		// load class model user_model
		$this->load->model('master_product_model','mpm');
		$this->load->model('type_reward_model','tr');
    }

    function index_get(){

        $action = $this->input->get('action');
        if($action != null || $action == ''){
            if($action == 'getAll'){

                $data = $this->mpm->getAllMasterProduct();

                $result = [
                    'status' => true,
                    'message' => '',
                    'data' => $data
                ];
                $this->set_response($result, REST_Controller::HTTP_OK);
            }
        }
    }

    function index_post(){
        $action = $this->input->post('action');
        $created_by = $this->input->post('created_by');
        $product_id = $this->input->post('product_id');
        $type_reward_id = $this->input->post('type_reward_id');
        $name_product = $this->input->post('name_product');
        $price = $this->input->post('price');
        $created_dt = date('Y-m-d');
        if($action != ''){
            if($action == 'insert'){
                if($created_by == '' || $created_by == null){
                    $result = [
                        'status' => false,
                        'message' => 'created  is required'
                    ];
                    $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }else if($name_product == '' || $name_product == null){
                    $result = [
                        'status' => false,
                        'message' => 'Name Product  is required'
                    ];
                    $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }else if($price == '' || $price == null){
                    $result = [
                        'status' => false,
                        'message' => 'Price Product  is required'
                    ];
                    $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }else{
                    $check_type_reward = $this->tr->getTypeRewardId($type_reward_id);
                    if(count($check_type_reward) > 0 && $check_type_reward != false){
                        //insert_update
                        $data = array(
                            'type_reward_id'    => $type_reward_id,
                            'name_product'      => $name_product,
                            'price'             => $price,
                            'created_by'        => $created_by,
                            'created_dt'        => $created_dt
                        );
                        $data = $this->mpm->insert_update($data,'');
                        $result = [
                            'status' => $data,
                            'message' => 'Successfull insert data',
                            'data' => array()
                        ];
                        $this->set_response($result, REST_Controller::HTTP_OK);
                    }else{
                        $result = [
                            'status' => false,
                            'message' => 'Master Type Reward not found'
                        ];
                        $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                    }
    
                }
            }
            else if('update'){
                if($product_id == null || $product_id == ''){
                    $result = [
                        'status' => false,
                        'message' => 'Product id is required'
                    ];
                    $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                }else{
                    $check_product_id = $this->mpm->getProductById($product_id);
                    if(count($check_product_id) > 0 && $check_product_id != false){
                        $data = array(
                            'type_reward_id'    => $type_reward_id,
                            'name_product'      => $name_product,
                            'price'             => $price,
                            'created_by'        => $created_by,
                            'created_dt'        => $created_dt
                        );
                        $data = $this->mpm->insert_update($data,$product_id);
                        $result = [
                            'status' => $data,
                            'message' => 'Successfull update data',
                            'data' => array()
                        ];
                        $this->set_response($result, REST_Controller::HTTP_OK);
                    }else{
                        $result = [
                            'status' => false,
                            'message' => 'Product id is not found'
                        ];
                        $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
                    }
                }
            }
        }else{
            $result = [
                'status' => false,
                'message' => 'param action is required'
            ];
            $this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}