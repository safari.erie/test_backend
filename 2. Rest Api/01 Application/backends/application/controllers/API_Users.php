<?php
defined('BASEPATH') OR exit('No direct script access allowed');


    
/* Created : safari.erie@gmail.com
 * Created_dt : 2019122-
 *
 * TODO
 * Login user with param username and password
 * retrieve all menu auth role login user

 *
 * History
 * ==================================================================
 * NO   | Date      | Description
 * ==================================================================
 * 1      20191220    Starting Class API Users
 * 2      20191220    
 * 3      20191220    
 * */

// load library REST Service
//require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/REST_Controller.php';

class API_users extends REST_Controller {

	// constructor to
	function __construct() 
    {
		parent:: __construct();
		// load class model user_model
		$this->load->model('users_model','um');
    }

	
    // index post
	function index_post(){

		// parameter for post client
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user_group_id = $this->input->post('user_group_id');
		$email = $this->input->post('email');
		$telephone = $this->input->post('telephone');
		$birth_place = $this->input->post('birth_place');
		$birth_date = $this->input->post('birth_date');
		$created_by = $this->input->post('created_by'); // can get data from session login 
		$changed_by = $this->input->post('changed_by'); // can get data from session login 
		$user_id = $this->input->post('user_id');
		$created_dt = date('Y-m-d');
		$action = $this->post('action');

		if($action != null || $action == ''){

			// send parameter for login login
			if($action == 'login'){
				// validate username empty
				if($username == '' || $username == null){
					$result = [
						'status' => false,
						'message' => 'username required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
				}
				 // validation password null
				else if($password == '' || $password == null){
					$result = [
						'status' => false,
						'message' => 'password required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
				}
				else{
					$check_user = $this->um->get_users_by_username($username);
					if(count($check_user) > 0 && $check_user != false){
						
						// validation input password diference with databases with encryption md5
						if($check_user->passwords != md5($password)){
							$result = [
								'status' => false,
								'message' => 'password incorect',
								'data' => array()
							];
							$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
						}else{
							$result = [
								'status'	=> true,
								'message'	=> 'success',
								'data'		=> $check_user
							];
	
							$this->set_response($result, REST_Controller::HTTP_OK);		
						}
										
					}
					 // check condition client input username, but in database not found
					else{
						$result = [
							'status' => false,
							'message' => 'username not registered',
							'data' => array()
						];
						$this->set_response($result, REST_Controller::HTTP_OK);
					}
				}
			}else if($action == 'create_users'){
				// validation user group id null
				if($user_group_id == '' || $user_group_id == null){
					$result = [
						'status' => false,
						'message' => 'user group required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
				}
				// validation email
				else if($email == '' || $email == null){
					$result = [
						'status' => false,
						'message' => 'email required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
				}
				// validation telephone
				else if($telephone == '' || $telephone == null){
					$result = [
						'status' => false,
						'message' => 'Telephone required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
				}
				// validation username null
				else if($username == '' || $username == null){
					$result = [
						'status' => false,
						'message' => 'username required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
				}
				 // validation password null
				 else if($password == '' || $password == null){
					$result = [
						'status' => false,
						'message' => 'password required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
				}else{
					
					$dt_user_group_id = $this->um->get_user_group_by_id($user_group_id);
					if(count($dt_user_group_id) > 0 && $dt_user_group_id != false){
						$data = array(
							'user_group_id'	=> $user_group_id,
							'username'		=> $username,
							'passwords'		=> md5($password),
							'email'			=> $email,
							'telephone'		=> $telephone,
							'birth_place'	=> $birth_place,
							'birth_date'	=> $birth_date,
							'created_by'	=> $created_by,
							'created_dt'	=> $created_dt
						);

						$insert = $this->um->insert_update_users($data,'');
						if($insert){
							$result = [
								'status' => $insert,
								'message' => 'Successfull insert data',
								'data' => array()
							];
							$this->set_response($result, REST_Controller::HTTP_OK);
						}else{
							$result = [
								'status' => false,
								'message' => 'Insert Failed',
								'data' => array()
							];
							$this->set_response($result, REST_Controller::HTTP_OK);
						}
						

					}else{
						$result = [
							'status' => false,
							'message' => 'User group id not found',
							'data' => array()
						];
						$this->set_response($result, REST_Controller::HTTP_OK);
					}
				}

			}else if($action == 'update_users'){
				if($user_id == '' || $user_id == null){
					$result = [
						'status' => false,
						'message' => 'password required',
						'data' => array()
					];
					$this->set_response($result, REST_Controller::HTTP_BAD_REQUEST);
				}else{
					$data = array(
						'user_group_id'	=> $user_group_id,
						'username'		=> $username,
						'passwords'		=> md5($password),
						'email'			=> $email,
						'telephone'		=> $telephone,
						'birth_place'	=> $birth_place,
						'birth_date'	=> $birth_date,
						'changed_by'	=> $changed_by,
						'changed_dt'	=> $created_dt
					);
					$update = $this->um->insert_update_users($data,$user_id);
					if($update){
						$result = [
							'status' => $update,
							'message' => 'Update Succesful data',
							'data' => array()
						];
						$this->set_response($result, REST_Controller::HTTP_OK);
					}else{
						$result = [
							'status' => false,
							'message' => 'failed update data',
							'data' => array()
						];
						$this->set_response($result, REST_Controller::HTTP_OK);
					}
				}
			}
			
		}

		

	}

	// index get
	function index_get(){

		$user_id = $this->input->get('user_id');
		$action 		  = $this->input->get('action');
		$status			  = false;
		$msg			  = '';
		$data			  = array();

		// cek action id
		if($action == 'id'){
			if($user_id == '' || $user_id == null ){
				$msg = 'User Id required';
			}else{
				$data_user_id = $this->um->get_user_by_id($user_id);
				if(count($data_user_id) > 0 && $data_user_id != false){
					// execution query
					$data_user = $this->um->get_user_by_id($user_id);
					$data['user'] = $data_user;
				}else{
					$msg = "User Id Not Found";
				}
			}
		}

		$result = [
			'status' => $status,
			'message' => $msg,
			'data' => $data
		];
		$this->set_response($result, REST_Controller::HTTP_OK);

	}

}
