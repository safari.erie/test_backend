<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$CI =& get_instance();

if( ! isset($_COOKIE['smartfix_language'])) {
	setcookie('smartfix_language', 'english', time()+3600, "/");
}
$lang = (isset($_COOKIE['smartfix_language']) && $_COOKIE['smartfix_language'] != '') ? 
			$_COOKIE['smartfix_language'] : 
			'english';
$CI->config->set_item('language', $lang);
	   
//template view render function
if ( ! function_exists('render'))
{
    function render($view = '', $vars = array())
    {
		$ci =& get_instance();
		
		$vars['_template'] = $view;
		$sidebar_view = APPPATH.'views/'.$view.'_sidebar.php';
		
		if(file_exists($sidebar_view)) {
			$vars['_sidebar'] = $view.'_sidebar';
		}
				
		$ci->load->view('layouts/header', $vars);
		$ci->load->view('layouts/sidebar_left', $vars);
		$ci->load->view('layouts/main', $vars);
		$ci->load->view('layouts/footer', $vars);
    }   
}

//template assets path helper
if ( ! function_exists('asset_path'))
{
    function asset_path($asset = '', $type = 'client')
    {
		// $type: 'client' or 'mechanic'
		if($type == 'client') 
		{
			$tpl_path = base_url('assets/template-client-administration/');
		}
		else 
		{
			$tpl_path = base_url('assets/template-mechanic/');
		}
        return $tpl_path . $asset;
    }   
}

//language helper
if ( ! function_exists('lang'))
{
    function lang($key = '')
    {
		$ci =& get_instance();
		$ci->lang->load('rest_controller_lang');
		
		return $key == '' ? '' : $ci->lang->line($key);
    }   
}

//get role function
if( ! function_exists('get_roles'))
{
	function get_roles()
	{
		$ci =& get_instance();
		$ci->load->model('login_model', 'lm');
		
		$checkUser = login_check();
		
		if($checkUser['status'] == false) return [];
		
		$roles = $ci->lm->get_role(
			$checkUser['data']['user_email'],
			$checkUser['data']['company_id']
		);
		
		return $roles;
	}
}

//login check helper
if ( ! function_exists('login_check'))
{
	function login_check($session_id = '')
    {
		$ci =& get_instance();
		
		if($session_id == '') {
			$session_id = $_COOKIE['smartfix_session_id'];
			if($session_id == '') return array(
				'status' => false,
				'message' => 'Session expired',
				'data'	=> array()
			);
		}
		
		$sql = "
			SELECT	 a.user_email, a.company_id, a.user_role_id, a.user_fullname, a.session_id, b.interface_lang	
			FROM			
				tb_m_user a	
				INNER JOIN tb_m_company b on b.company_id=a.company_id 	
			WHERE			
				a.email_invited = '1'		
				and a.session_id = '{$session_id}'
		";
		
		$q = $ci->db->query($sql);
		
		if($q->num_rows() > 0){
			return array(
				'status' => true,
				'message' => 'Already logged in',
				'data'	=> $q->result_array()[0]
			);
		}else{
			return array(
				'status' => false,
				'message' => 'Session expired',
				'data'	=> array()
			);
		}
	}
}

//call api function
if(! function_exists('callAPI'))
{
	function callAPI($method, $url, $data, $session_id = '')
	{
	   $curl = curl_init();

	   switch ($method){
		  case "POST":
			 curl_setopt($curl, CURLOPT_POST, 1);
			 if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			 break;
		  case "PUT":
			 curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			 if ($data)
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
			 break;
		  default:
			 if ($data)
				$url = sprintf("%s?%s", $url, http_build_query($data));
	   }

	   // OPTIONS:
	   curl_setopt($curl, CURLOPT_URL, $url);
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		  'authorization: '.$session_id,
		  'Content-Type: application/json',
	   ));
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	   // EXECUTE:
	   $result = curl_exec($curl);
	   if(!$result){die("Connection Failure");}
	   curl_close($curl);
	   return $result;
	}
}